<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mediaxinstitucion extends Model
{
        //// Tabla en la base de datos
    protected $table = 'mediaxinstitucion'; // Carrera es el nombre correcto
    public $incrementing = false;

    // Primary key
    protected $primaryKey = 'id';

    // No se guarde el tiempo y la fecha del registro
    public $timestamps = false;

    protected $fillable = [
    	'id', 'media_link', 'tipo', 'institucion','titulo'
    ];

    protected $guarded = []; // Vacío
}
