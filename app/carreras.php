<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class carreras extends Model
{
	// Tabla en la base de datos
    protected $table = 'carreras'; // Carrera es el nombre correcto
    public $incrementing = false;

    // Primary key
    protected $primaryKey = 'id';

    // No se guarde el tiempo y la fecha del registro
    public $timestamps = false;

    protected $fillable = [
    	'university', 'nombre_carrera', 'tipo', 'pensum', 'periodo', 'perfil', 'desem', 'tipo_test','ruc', 'id'
    ];

    protected $guarded = []; // Vacío

}