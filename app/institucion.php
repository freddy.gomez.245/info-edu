<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class institucion extends Model
{
        //// Tabla en la base de datos
    protected $table = 'institucion'; // Carrera es el nombre correcto
    public $incrementing = false;

    // Primary key
    protected $primaryKey = 'ruc';

    // No se guarde el tiempo y la fecha del registro
    public $timestamps = false;

    protected $fillable = [
    	'ruc', 'nombre', 'acronimo', 'mision','vision', 'logo','web', 'tipo'
    ];

    protected $guarded = []; // Vacío
}
