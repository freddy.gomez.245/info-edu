<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class recinto extends Model
{
       //// Tabla en la base de datos
    protected $table = 'recinto'; // Carrera es el nombre correcto
    public $incrementing = false;

    // Primary key
    protected $primaryKey = 'codigo';

    // No se guarde el tiempo y la fecha del registro
    public $timestamps = false;

    protected $fillable = [
    	'codigo', 'nombre', 'acroniimo', 'institucion','webpage', 'latitud','longitud', 'Dir'
    ];

    protected $guarded = []; // Vacío
}
