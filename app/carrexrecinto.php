<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class carrexrecinto extends Model
{
	// Tabla en la base de datos
    protected $table = 'carrexrecinto'; // Carrera es el nombre correcto
    public $incrementing = false;

    // Primary key


    // No se guarde el tiempo y la fecha del registro
    public $timestamps = false;

    protected $fillable = [
    	'carrera', 'recinto'
    ];

    protected $guarded = []; // Vacío


}
